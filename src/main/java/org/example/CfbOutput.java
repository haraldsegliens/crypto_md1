package org.example;

public class CfbOutput {
    private byte[] dataBytes;
    private byte[] cmacBytes;

    public CfbOutput() {
    }

    public byte[] getDataBytes() {
        return dataBytes;
    }

    public void setDataBytes(byte[] dataBytes) {
        this.dataBytes = dataBytes;
    }

    public byte[] getCmacBytes() {
        return cmacBytes;
    }

    public void setCmacBytes(byte[] cmacBytes) {
        this.cmacBytes = cmacBytes;
    }
}
