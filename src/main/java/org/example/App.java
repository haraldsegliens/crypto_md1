package org.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Date;

public class App {
    public static int blockSizeInBytes = 16;//AES-64

    public static void main( String[] args ) {
        if(args.length >= 3 && args[0].equals("generate_key")) {
            String passphrase = args[1];
            String keyFileLocation = args[2];

            var keyBytes = new KeyGenerator()
                    .generateKey(passphrase);
            var keyHexString = HexUtils.bytesToHexString(keyBytes);

            try {
                Files.write(Paths.get(keyFileLocation), keyHexString.getBytes());
            } catch (IOException e) {
                throw new RuntimeException("Couldn't save a key!", e);
            }
        } else if(args.length >= 4 && args[0].equals("encrypt_CBC")) {
            String keyFileLocation = args[1];
            String cipherFileLocation = args[2];
            String plaintextFileLocation = args[3];

            String keyHexString;
            try {
                keyHexString = new String(Files.readAllBytes(Paths.get(keyFileLocation)));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read a key!", e);
            }
            var keyBytes = HexUtils.hexStringToBytes(keyHexString);

            byte[] plaintextBytes;
            try {
                plaintextBytes = Files.readAllBytes(Paths.get(plaintextFileLocation));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read plaintext!", e);
            }

            byte[] iv = new byte[blockSizeInBytes];
            Arrays.fill(iv, (byte) 0); //fill IV with 0

            byte[] cipherBytes = new AlgorithmAESCBC(BlockProcessType.ENCRYPTION, keyBytes)
                    .encryptCBC(plaintextBytes, iv);

            try {
                Files.write(Paths.get(cipherFileLocation), cipherBytes);
            } catch (IOException e) {
                throw new RuntimeException("Couldn't save cipher!", e);
            }
        } else if(args.length >= 4 && args[0].equals("decrypt_CBC")) {
            String keyFileLocation = args[1];
            String cipherFileLocation = args[2];
            String encryptedPlaintextFileLocation = args[3];

            String keyHexString;
            try {
                keyHexString = new String(Files.readAllBytes(Paths.get(keyFileLocation)));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read a key!", e);
            }
            var keyBytes = HexUtils.hexStringToBytes(keyHexString);

            byte[] cipherBytes;
            try {
                cipherBytes = Files.readAllBytes(Paths.get(cipherFileLocation));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read cipher!", e);
            }

            byte[] iv = new byte[blockSizeInBytes];
            Arrays.fill(iv, (byte) 0); //fill IV with 0

            byte[] encryptedPlaintextBytes = new AlgorithmAESCBC(BlockProcessType.DECRYPTION, keyBytes)
                    .decryptCBC(cipherBytes, iv);

            try {
                Files.write(Paths.get(encryptedPlaintextFileLocation), encryptedPlaintextBytes);
            } catch (IOException e) {
                throw new RuntimeException("Couldn't save encrypted plaintext!", e);
            }
        } else if(args.length >= 8 && args[0].equals("encrypt_CFB")) {
            String keyFileLocation = args[1];
            String cmacKeyFileLocation = args[2];
            int cmacTagSizeInBytes = Integer.parseInt(args[3]);
            String cmacTagFileLocation = args[4];
            String cipherFileLocation = args[5];
            String plaintextFileLocation = args[6];
            String ivFileLocation = args[7];

            String keyHexString;
            try {
                keyHexString = new String(Files.readAllBytes(Paths.get(keyFileLocation)));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read a key!", e);
            }
            var keyBytes = HexUtils.hexStringToBytes(keyHexString);

            String cmacKeyHexString;
            try {
                cmacKeyHexString = new String(Files.readAllBytes(Paths.get(cmacKeyFileLocation)));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read a cmacKey!", e);
            }
            var cmacKeyBytes = HexUtils.hexStringToBytes(cmacKeyHexString);

            byte[] plaintextBytes;
            try {
                plaintextBytes = Files.readAllBytes(Paths.get(plaintextFileLocation));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read plaintext!", e);
            }

            byte[] iv = new byte[blockSizeInBytes];
            new SecureRandom(new Date().toString().getBytes())
                .nextBytes(iv);//fill IV with random

            CfbOutput encryptOutput = new AlgorithmAESCFB(BlockProcessType.ENCRYPTION, keyBytes, cmacKeyBytes)
                    .encryptCFB(plaintextBytes, iv, cmacTagSizeInBytes);

            try {
                Files.write(Paths.get(cipherFileLocation), encryptOutput.getDataBytes());
            } catch (IOException e) {
                throw new RuntimeException("Couldn't save cipher!", e);
            }

            try {
                Files.write(Paths.get(cmacTagFileLocation), encryptOutput.getCmacBytes());
            } catch (IOException e) {
                throw new RuntimeException("Couldn't save cmac tag file!", e);
            }

            try {
                Files.write(Paths.get(ivFileLocation), iv);
            } catch (IOException e) {
                throw new RuntimeException("Couldn't save IV file!", e);
            }
        } else if(args.length >= 7 && args[0].equals("decrypt_CFB")) {
            String keyFileLocation = args[1];
            String cmacKeyFileLocation = args[2];
            String cmacTagFileLocation = args[3];
            String cipherFileLocation = args[4];
            String encryptedPlaintextFileLocation = args[5];
            String ivFileLocation = args[6];

            String keyHexString;
            try {
                keyHexString = new String(Files.readAllBytes(Paths.get(keyFileLocation)));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read a key!", e);
            }
            var keyBytes = HexUtils.hexStringToBytes(keyHexString);

            String cmacKeyHexString;
            try {
                cmacKeyHexString = new String(Files.readAllBytes(Paths.get(cmacKeyFileLocation)));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read a cmacKey!", e);
            }
            var cmacKeyBytes = HexUtils.hexStringToBytes(cmacKeyHexString);

            byte[] cmacTagBytes;
            try {
                cmacTagBytes = Files.readAllBytes(Paths.get(cmacTagFileLocation));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read cmacTag!", e);
            }

            byte[] cipherBytes;
            try {
                cipherBytes = Files.readAllBytes(Paths.get(cipherFileLocation));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read cipher!", e);
            }

            byte[] iv;
            try {
                iv = Files.readAllBytes(Paths.get(ivFileLocation));
            } catch (IOException e) {
                throw new RuntimeException("Couldn't read cipher!", e);
            }

            CfbOutput cfbOutput = new AlgorithmAESCFB(BlockProcessType.DECRYPTION, keyBytes, cmacKeyBytes)
                    .decryptCFB(cipherBytes, iv, cmacTagBytes);

            try {
                Files.write(Paths.get(encryptedPlaintextFileLocation), cfbOutput.getDataBytes());
            } catch (IOException e) {
                throw new RuntimeException("Couldn't save encrypted plaintext!", e);
            }
        } else {
            System.out.println("generate_key <passphrase> <key_file_location> - generate AES key from a passphrase");
            System.out.println("encrypt_CBC <key_file_location> <cipher_file_location> <plain_text_file_location> - encrypt plaintext with CBC");
            System.out.println("decrypt_CBC <key_file_location> <cipher_file_location> <encrypted_plain_text_file_location> - decrypt plaintext with CBC");
            System.out.println("encrypt_CFB <key_file_location> <cmac_key_file_location> <cmac_tag_size_in_bytes> <cmac_tag_file_location> <cipher_file_location> <plain_text_file_location> <iv_file_location> - encrypt plaintext with CFB");
            System.out.println("decrypt_CFB <key_file_location> <cmac_key_file_location> <cmac_tag_file_location> <cipher_file_location> <encrypted_plain_text_file_location> <iv_file_location> - decrypt plaintext with CFB");
        }
    }
}
