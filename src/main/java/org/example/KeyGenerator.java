package org.example;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class KeyGenerator {
    public KeyGenerator() {}

    public byte[] generateKey(String passphrase) {
        SecretKeyFactory factory;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Couldn't find SecretKeyFactory");
        }
        KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), new byte[]{0, 0, 0}, 65536, App.blockSizeInBytes * 8);
        try {
            SecretKey secretKey = factory.generateSecret(spec);
            return secretKey.getEncoded();
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Couldn't generate secret!");
        }
    }
}
