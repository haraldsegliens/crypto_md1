package org.example;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class AlgorithmAESCFB {
    private final Cipher cipher;
    private final BlockProcessType processType;
    private final byte[] cmacKey;

    public AlgorithmAESCFB(BlockProcessType processType, byte[] key, byte[] cmacKey) {
        this.processType = processType;
        this.cmacKey = cmacKey;
        try {
            cipher = Cipher.getInstance("AES/ECB/NoPadding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get cipher!", e);
        }

        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

        try {
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        } catch (InvalidKeyException e) {
            throw new RuntimeException("Failed to init cipher", e);
        }
    }

    private byte[] getMostSignificantBits(byte[] data, int bits) {
        int[] b = new int[]{0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfe, 0xff};

        byte[] outputBytes = new byte[(int) Math.ceil((double)bits / 8)];
        int srcPos = data.length - (bits / 8);
        int destPos = outputBytes.length - (bits / 8);
        System.arraycopy(data, srcPos, outputBytes, destPos, bits / 8);
        if(destPos == 1) {
            outputBytes[0] = (byte) (data[srcPos - 1] & b[bits%8 - 1]);
        }
        return outputBytes;
    }

    private byte[] getPaddedData(byte[] allData) {
        //making paddedAllData from allData that is padded with 0
        byte[] paddedAllData = new byte[allData.length + App.blockSizeInBytes - (allData.length % App.blockSizeInBytes)];
        System.arraycopy(allData, 0, paddedAllData, 0, allData.length);
        Arrays.fill(paddedAllData, allData.length, paddedAllData.length, (byte) 0);
        return paddedAllData;
    }

    public CfbOutput encryptCFB(byte[] allData, byte[] iv, int cmacTagSizeInBytes) {
        if(iv.length != App.blockSizeInBytes) {
            throw new RuntimeException("IV is invalid length!");
        }

        byte[] paddedAllData = getPaddedData(allData);

        byte[] previousCipher = new byte[App.blockSizeInBytes];
        System.arraycopy(iv, 0, previousCipher, 0, iv.length);

        byte[] cipherBytes = new byte[paddedAllData.length];
        for(int i = 0; i < paddedAllData.length; i+=App.blockSizeInBytes) {
            //block cipher encryption
            previousCipher = aesBlock(previousCipher);

            //previousCipher xor plaintext
            for(int j = 0, k = i; j < App.blockSizeInBytes; j++, k++) {
                previousCipher[j] = (byte) (paddedAllData[k] ^ previousCipher[j]);
            }

            //put previousCipher in output
            System.arraycopy(previousCipher, 0, cipherBytes, i, App.blockSizeInBytes);
        }

        byte[] cmacTagBytes = generateCmacTag(paddedAllData, cmacTagSizeInBytes);

        var output = new CfbOutput();
        output.setDataBytes(cipherBytes);
        output.setCmacBytes(cmacTagBytes);
        return output;
    }

    public CfbOutput decryptCFB(byte[] allData, byte[] iv, byte[] cmacTagBytes) {
        if(iv.length != App.blockSizeInBytes) {
            throw new RuntimeException("IV is invalid length!");
        }

        byte[] previousCipher = new byte[App.blockSizeInBytes];
        System.arraycopy(iv, 0, previousCipher, 0, iv.length);

        byte[] paddedAllData = new byte[allData.length];
        byte[] plaintextBuffer;
        for(int i = 0; i < allData.length; i+=App.blockSizeInBytes) {
            //block cipher decryption
            plaintextBuffer = aesBlock(previousCipher);

            //read currentCipher in previousCipher
            System.arraycopy(allData, i, previousCipher, 0, App.blockSizeInBytes);

            //plaintextBuffer xor currentCipher
            for(int j = 0; j < App.blockSizeInBytes; j++) {
                plaintextBuffer[j] = (byte) (plaintextBuffer[j] ^ previousCipher[j]);
            }

            //put plaintextBuffer in output
            System.arraycopy(plaintextBuffer, 0, paddedAllData, i, App.blockSizeInBytes);
        }

        byte[] cmacTagBytes1 = generateCmacTag(paddedAllData, cmacTagBytes.length);
        if(!Arrays.equals(cmacTagBytes,cmacTagBytes1)) {
            throw new RuntimeException("CMAC tags mismatch!");
        }

        var output = new CfbOutput();
        output.setDataBytes(paddedAllData);
        return output;
    }

    private byte[] aesBlock(byte[] data) {
        try {
            return cipher.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException("Failed to process the block with cipher", e);
        }
    }

    private byte[] generateCmacTag(byte[] paddedAllData, int cmacTagSizeInBytes) {
        byte[] cmacTagBytes = new byte[App.blockSizeInBytes];
        //calculate cmac tag buffer from all plaintext message block except the last block
        for(int i = 0; i < paddedAllData.length - App.blockSizeInBytes; i+=App.blockSizeInBytes) {
            for(int j = 0, k = i; j < App.blockSizeInBytes; j++, k++) {
                cmacTagBytes[j] = (byte) (cmacTagBytes[j] ^ paddedAllData[k]);
            }
            cmacTagBytes = aesBlock(cmacTagBytes);
        }
        //calculate cmac tag buffer from the last message block
        for(int j = 0, k = paddedAllData.length - App.blockSizeInBytes; j < App.blockSizeInBytes; j++, k++) {
            cmacTagBytes[j] = (byte) (cmacTagBytes[j] ^ paddedAllData[k] ^ cmacKey[j]);
        }
        //get cmac tag from buffer
        return getMostSignificantBits(cmacTagBytes, cmacTagSizeInBytes * 8);
    }
}
