package org.example;

public enum BlockProcessType {
    ENCRYPTION,
    DECRYPTION
}
