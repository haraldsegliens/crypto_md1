package org.example;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class AlgorithmAESCBC {
    private final Cipher cipher;
    private final BlockProcessType processType;

    public AlgorithmAESCBC(BlockProcessType processType, byte[] key) {
        this.processType = processType;
        try {
            cipher = Cipher.getInstance("AES/ECB/NoPadding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get cipher!", e);
        }

        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

        try {
            if(processType == BlockProcessType.ENCRYPTION) {
                cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            } else {
                cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            }
        } catch (InvalidKeyException e) {
            throw new RuntimeException("Failed to init cipher", e);
        }
    }

    private byte[] getPaddedData(byte[] allData) {
        //making paddedAllData from allData that is padded with 0
        byte[] paddedAllData = new byte[allData.length + App.blockSizeInBytes - (allData.length % App.blockSizeInBytes)];
        System.arraycopy(allData, 0, paddedAllData, 0, allData.length);
        Arrays.fill(paddedAllData, allData.length, paddedAllData.length, (byte) 0);
        return paddedAllData;
    }

    public byte[] encryptCBC(byte[] allData, byte[] iv) {
        if(iv.length != App.blockSizeInBytes) {
            throw new RuntimeException("IV is invalid length!");
        }

        byte[] paddedAllData = getPaddedData(allData);

        byte[] previousCipher = new byte[App.blockSizeInBytes];
        System.arraycopy(iv, 0, previousCipher, 0, iv.length);

        byte[] output = new byte[paddedAllData.length];
        for(int i = 0; i < paddedAllData.length; i+=App.blockSizeInBytes) {
            //previousCipher xor plaintext
            for(int j = 0, k = i; j < App.blockSizeInBytes; j++, k++) {
                previousCipher[j] = (byte) (previousCipher[j] ^ paddedAllData[k]);
            }

            //block cipher encryption
            previousCipher = aesBlock(previousCipher);

            //put previousCipher in output
            System.arraycopy(previousCipher, 0, output, i, App.blockSizeInBytes);
        }
        return output;
    }

    public byte[] decryptCBC(byte[] allData, byte[] iv) {
        if(iv.length != App.blockSizeInBytes) {
            throw new RuntimeException("IV is invalid length!");
        }

        byte[] previousCipher = new byte[App.blockSizeInBytes];
        System.arraycopy(iv, 0, previousCipher, 0, iv.length);

        byte[] output = new byte[allData.length];
        byte[] plaintextBuffer;
        for(int i = 0; i < allData.length; i+=App.blockSizeInBytes) {
            byte[] currentCipher = new byte[App.blockSizeInBytes];
            System.arraycopy(allData, i, currentCipher, 0, App.blockSizeInBytes);

            //block cipher decryption
            plaintextBuffer = aesBlock(currentCipher);

            //previousCipher xor plaintextBuffer
            for(int j = 0; j < App.blockSizeInBytes; j++) {
                plaintextBuffer[j] = (byte) (previousCipher[j] ^ plaintextBuffer[j]);
            }

            //put plaintextBuffer in output
            System.arraycopy(plaintextBuffer, 0, output, i, App.blockSizeInBytes);

            //set currentCipher as previousCipher
            previousCipher = currentCipher;
        }
        return output;
    }

    private byte[] aesBlock(byte[] data) {
        try {
            return cipher.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException("Failed to process the block with cipher", e);
        }
    }
}
