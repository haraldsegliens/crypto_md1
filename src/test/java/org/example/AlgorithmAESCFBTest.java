package org.example;

import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class AlgorithmAESCFBTest extends TestCase {
    static final Logger logger = LoggerFactory.getLogger(AlgorithmAESCFBTest.class);

    public void testProcess() {
        String keyHex = "0123456789ABCDEF0123456789ABCDEF";
        String cmacKeyHex = "0123456789ABCDEF0123456789ABCDEF";
        String plainText = "Hello world. My name is Haralds Egliens.";

        byte[] iv = new byte[App.blockSizeInBytes];
        Arrays.fill(iv, (byte) 0); //fill IV with 0

        logger.info("plainText: {}", plainText);

        var encryptor = new AlgorithmAESCFB(BlockProcessType.ENCRYPTION, HexUtils.hexStringToBytes(keyHex), HexUtils.hexStringToBytes(cmacKeyHex));
        var decryptor = new AlgorithmAESCFB(BlockProcessType.DECRYPTION, HexUtils.hexStringToBytes(keyHex), HexUtils.hexStringToBytes(cmacKeyHex));

        CfbOutput encryptOutput = encryptor.encryptCFB(plainText.getBytes(), iv, 4);

        logger.info("cipherBytes: {}", HexUtils.bytesToHexString(encryptOutput.getDataBytes()));
        logger.info("cmacBytes: {}", HexUtils.bytesToHexString(encryptOutput.getCmacBytes()));

        CfbOutput decryptOutput = decryptor.decryptCFB(encryptOutput.getDataBytes(), iv, encryptOutput.getCmacBytes());
        String encryptedPlaintext = new String(decryptOutput.getDataBytes());

        logger.info("encryptedPlaintext: {}", encryptedPlaintext);
    }
}