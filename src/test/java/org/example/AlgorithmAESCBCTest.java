package org.example;

import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class AlgorithmAESCBCTest extends TestCase {
    static final Logger logger = LoggerFactory.getLogger(AlgorithmAESCBCTest.class);

    public void testProcess() {
        String keyHex = "0123456789ABCDEF0123456789ABCDEF";
        String plainText = "Hello world. My name is Haralds Egliens.";

        byte[] iv = new byte[App.blockSizeInBytes];
        Arrays.fill(iv, (byte) 0); //fill IV with 0

        logger.info("plainText: {}", plainText);

        var encryptor = new AlgorithmAESCBC(BlockProcessType.ENCRYPTION, HexUtils.hexStringToBytes(keyHex));
        var decryptor = new AlgorithmAESCBC(BlockProcessType.DECRYPTION, HexUtils.hexStringToBytes(keyHex));

        byte[] cipherBytes = encryptor.encryptCBC(plainText.getBytes(), iv);

        logger.info("cipherBytes: {}", HexUtils.bytesToHexString(cipherBytes));

        byte[] encryptedPlaintextBytes = decryptor.decryptCBC(cipherBytes, iv);
        String encryptedPlaintext = new String(encryptedPlaintextBytes);

        logger.info("encryptedPlaintext: {}", encryptedPlaintext);

    }
}