# crypto_md1

Homework 1 for course "DatZ6015 : Applied cryptography"

## Notes

JAVA 11

## Commands

1. generate_key <passphrase> <key_file_location>
2. encrypt_CBC <key_file_location> <cipher_file_location> <plain_text_file_location>
3. decrypt_CBC <key_file_location> <cipher_file_location> <encrypted_plain_text_file_location>
4. encrypt_CFB <key_file_location> <cmac_key_file_location> <cmac_tag_size_in_bytes> <cmac_tag_file_location> <cipher_file_location> <plain_text_file_location> <iv_file_location>
5. decrypt_CFB <key_file_location> <cmac_key_file_location> <cmac_tag_file_location> <cipher_file_location> <encrypted_plain_text_file_location> <iv_file_location>